function filter(elements, cb ){
    var newArray = []
    for (let index = 0; index < elements.length; index++){
        if (cb(elements[index],index)) {
            newArray.push(elements[index])
        }
    }
    return newArray
}
module.exports = filter 