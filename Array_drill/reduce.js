function reduce(elements, cb) {
    var intialValue = 0

    for (let index = 0; index < elements.length; index ++){
        intialValue = cb(intialValue, elements[index])
    }
    
    return intialValue
}
module.exports = reduce


