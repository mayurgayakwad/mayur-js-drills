const inventory = require('./inventory.js')

function problem1() {

const id = 33
var len = inventory.length

for (let i = 0; i < len; i++)
{
    if (inventory[i]['id'] === id )
    {
        return ('Car 33 is a ' +  inventory[i]["car_year"] + ' '+ inventory[i]["car_make"] + " " +   inventory[i]["car_model"])
    }
}
    
 }
module.exports = problem1
