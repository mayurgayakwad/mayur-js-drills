function defaults(obj, defaultProps){
    
    for(key in defaultProps){
        
        if(!(key in obj)){
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}
module.exports=defaults;
